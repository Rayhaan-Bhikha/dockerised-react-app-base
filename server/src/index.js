const express = require('express');
const app = express();
const path = require('path');

const port = process.env.PORT || 8000;

console.log(process.env.NODE_ENV);

if(process.env.NODE_ENV === "prod") {
    app.use(express.static(path.resolve('..','dist')));

    app.get('/', (req, res) => {
        res.sendFile('index');
    })
} else if (process.env.NODE_ENV === "test") {
    /* 
        morgan used to print out information to console 
        when api request is made to the server. 
    */
    var morgan = require("morgan");
    app.use(morgan('tiny'));
}

var example = 5;

app.use('/api', require('./api'));

app.listen(port, () => {
    console.log(`server started on port: ${port}`);
})