const express = require('express');
const app = express.Router();

var userData = [
    {
        id: 1,
        name: "Rayhaan Bhikha",
        gender: "Male"
    }, 
    {
        id: 2,
        name: "Ione Murray",
        gender: "Female"
    },
    {
        id: 3,
        name: "Edward Andrews",
        gender: "Male"
    },
    {
        id: 4,
        name: "Andrew Grierson",
        gender: "Male"
    },
    {
        id: 5,
        name: "Sarah Bannister",
        gender: "Female"
    },
    {
        id: 6,
        name: "Oliver Martin-Hirsch",
        gender: "Male"
    }
]

app.get("/users", (req, res) => {
    res.status(200).json(userData);
})

module.exports = app;