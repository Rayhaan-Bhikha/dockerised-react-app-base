import React from 'react'

export default class Example extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            userData: null
        }
    }

    handleClick = () => {
       fetch("/api/users")
        .then(resp => resp.json())
        .then(data => this.setState({
            userData: data
        }));
    }

    getUsers = () => {
        if(this.state.userData != null) {
            const usersList = this.state.userData.map(user => 
                <li key={user.id}>name: {user.name}: gender: {user.gender} </li>
            );
            return usersList
        }
        return null;
    }

    render() {
        return (
            <div>
                Example Api:
                <button onClick={this.handleClick}> Get Users </button>
                <ul>{this.getUsers()}</ul>
            </div>
        )
    }
}