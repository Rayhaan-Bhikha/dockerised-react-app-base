import React from 'react'
import {render} from 'react-dom'

import {Example} from 'Components'

class App extends React.Component{
    render() {
        return (
            <div>
                Hello world!
                <Example />
            </div>
        )
    }
}

render(<App />, document.getElementById('root'));