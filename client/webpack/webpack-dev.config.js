const path = require('path');
const webpack = require('webpack');
const baseConfig = require('./webpack.config.js');
const merge = require('webpack-merge');

const proxyUrl = process.env.proxyUrl || 'localhost';

const devConfig = {
    mode: "development",
    devtool: 'inline-source-map',
    devServer: {
        contentBase: path.resolve('dist'),
        hot: true,
        port: 9000,
        inline: true,
        host: '0.0.0.0',
        proxy: {
            '/api':'http://'+ proxyUrl + ':8000'
        }
    },
    plugins: [
        new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin()
    ]
}

module.exports = merge(baseConfig, devConfig);