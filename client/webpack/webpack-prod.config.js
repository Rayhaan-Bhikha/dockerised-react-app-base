const baseConfig = require('./webpack.config.js');
const merge = require('webpack-merge');


const prodConfig = {
    mode: "production",
    devtool: 'source-map'
}

module.exports = merge(baseConfig, prodConfig);