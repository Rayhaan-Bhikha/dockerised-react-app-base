const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const SRC_DIR = path.resolve("src");

const config = {
    entry: ["babel-polyfill", path.join(SRC_DIR, "App.jsx")],
    output: {
        path: path.resolve("dist"),
        filename: "bundle.js"
    },
    module: {
        rules: [{
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: "babel-loader",
                options: {
                    presets: ["env", "react", "stage-2"],
                    plugins: [require("babel-plugin-transform-class-properties")]
                }
            },
            {
                test: /\.scss$/,
                use: [
                    "style-loader", // creates style nodes from JS strings
                    "css-loader", // translates CSS into CommonJS
                    "sass-loader" // compiles Sass to CSS
                ]
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(['dist']), // should be used if we use hashes in bundles.js files
        new HtmlWebpackPlugin({
            template: path.join(SRC_DIR, 'index.html')
        })
    ],
    resolve: {
        alias: {
            Components: path.join(SRC_DIR, "components"),
            Services: path.join(SRC_DIR, "services")
        }
    }
}

module.exports = config;