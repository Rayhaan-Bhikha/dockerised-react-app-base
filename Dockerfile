# Pull node image from docker hub - stage
# ==========================
FROM node:8.11.3 as base

# front end Stage
# ==========================
FROM base as frontend

# set working directory.
WORKDIR /kaizen-frontend

# copy src test package.json etc.
COPY  ./client ./

# install dependencies
RUN npm install

EXPOSE 9000

# clean up


# Back end Stage
# ==========================
FROM base as backend

# set working directory.
WORKDIR /kaizen-backend

# copy src test package.json etc.
COPY  ./server ./

# install dependencies
RUN npm install

# clean up


# Production stage.
# =========================
FROM base as production

WORKDIR /kaizen-production

COPY --from=backend /kaizen-backend ./server

COPY --from=frontend /kaizen-frontend ./client

WORKDIR client

RUN npm run client:prod && \
    mv dist/ ../dist

WORKDIR /kaizen-production/server

CMD ["npm", "run", "server:prod"]