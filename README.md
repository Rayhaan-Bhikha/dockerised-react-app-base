# To get started

This application uses npm to pull external libraries/frameworks into the application.

To install all the dependencies needed in the application run the following command in the root directory:

```
npm install
```

To run the application, run the following command in the root directory: 

```
npm start
```

This should start the server on port `8000`. 

In the browser go to http://localhost:8000 and you should see a dummy application running which calls a back-end api.

# Environments

## Production 

In production to build the **dist** directory:

```
npm run client:prod
```

You should see the **dist** folder appear.

# Development

During development you can imagine while your testing your code it would be a pain to constantly compile your js files. To tackle this webpack has some dev configurations which can be used to hot reload any changes you make. At the same time you also want your node server running (just in case your using any back-end api's).

To do this, the idea is you seperate the two during development.

The node server (back-end/api server) and the webpack-dev-server (which is technically your front-end) run seperately.

To see this in action, run the following commands in the root directory.

## Webpack-dev-server 

```
npm run client:dev
```
This should start a webpack-dev-server on port `9000`.

With the webpack-dev-server running you can see your react app by visiting http://localhost:9000

---

## Node server

Now in a different terminal window, run the folloeing command to start the node server.

```
npm run server:dev

```

This should start the server on port `8000`.



